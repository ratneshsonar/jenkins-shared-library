#!/usr/bin/env groovy

def call()  {
    echo 'Building the docker image..'
    withCredentials([usernamePassword(credentialsId: 'Docker-details', passwordVariable: 'PASS', usernameVariable: 'USER')])   {
        sh 'docker build -t ratneshsonar/repo1:jma-1.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push ratneshsonar/repo1:jma-1.0'
    }
}